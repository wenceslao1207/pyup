from .classmodule import systems

def pi():
    pc = systems('pi', '192.168.0.100', 'Lordlao1207')
    pc.execute_command("sudo apt update -y")
    pc.execute_command("sudo apt upgrade -y")

def frankenpc():
    pc = systems('root', '192.168.0.107', 'wenceslao1207')
    pc.execute_command('xbps-install -Syyu')

def desktop():
    pc = systems('root', '192.168.0.127', 'Lordlao-1207|Wenceslao')
    pc.execute_command('dnf update -y')

def shutdownall():
    pc = systems('root', '192.168.0.127', 'Lordlao-1207|Wenceslao')
    pc.execute_command('shutdown -h now')
    pc = systems('root', '192.168.0.107', 'wenceslao1207')
    pc.execute_command('shutdown -h now')
    pc = systems('pi', '192.168.0.100', 'Lordlao1207')
    pc.execute_command("sudo shutdown -h now")
