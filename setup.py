from setuptools import setup

setup(
    name='pyup',
    version='0.1.0',
    packages=['cli'],
    install_requires=[
        'paramiko'
    ],
    entry_points = {
        'console_scripts':[
            'pyup = cli.__main__:main'
        ]
    })


